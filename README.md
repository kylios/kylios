# Development

First, make sure you have `ruby` and `ruby-dev` installed.

```
sudo apt install ruby ruby-dev
```

You need `ruby-bundler` in order to build and run the website.

```
sudo apt install ruby-bundler
```

Now, install the site packages:

```
bundle install --path vendor
```

This will install all the site's dependencies. You can now run the development
server:

```
bundle exec jekyll serve
```
