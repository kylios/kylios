---
layout: post
title: The Ghost of Old Rotham's Sawmill
date: June 20, 2018
categories:
  - writing
  - stories
description: |
  A runaway slave, fighting for her freedom, protects her hideout with a curse.
  Two brothers experience firsthand the lengths she goes to.
introduction: |
  This was my entry for the first challenge of NYC Midnight's 2018 Flash Fiction Competition. My prompt here was to write a ghost story, set at a waterfall, and incorporating a salt shaker, somehow. Like always, I felt pretty rushed here, but there are some aspects of the story I really like, such as the dialog between the brothers. My weakness is plotting, so I didn't have a great backstory, and I really wish this story could dive into the slave family a bit more and tidy up the reasons why the waterfall is haunted.

  I will be awaiting the results from the judges as I prepare for the second challenge a little later this year.
---

Charlie’s skeleton face reflected the ghostly moonlight as he lay, deathly still, on the bed beside the window. The nurse had woke me to tell me this could be my last chance to see him alive. I stood in the doorway and thought of Mom and Dad – what would they have thought right now? Would they have been proud of us?

“Charlie,” I whispered. I saw his eye open, and moved forward to kneel beside him.

“I’ll be with them soon,” he said with failing voice. Ever since his childhood, Charlie became strangely spiritual, speaking of ghosts and spirits as if they were all around us, and Heaven was the only escape.

I reached up and touched his shoulder. His hand was tucked deeply under the covers, and I could see him shifting, fumbling.

“Mom and Dad,” he said. “I know what happened to them.”

“Charlie, they died years ago in a freak car accident. We both know that.”

“No,” he replied. “What _really_ happened.”

I wanted to tell him, _enough with the ghost stories_, but this moment was his.

“Tell me, Charlie.”

“Remember the waterfall? The one at Rotham’s Lake, by the old sawmill?”

I nodded; of course I remembered. They’d always said there was a haunted cave behind that waterfall. A family of slaves worked the sawmill up the creek. One day they revolted, killed their master, and fled to that cave where the dogs couldn’t sniff them out. The story goes that they slipped and died on those rocks, all but the mother, who haunts anyone who comes near.

“I made it inside the cave,” said Charlie. “After you left for college, in the fall, before it got too cold, I went back to the waterfall. I climbed the rocks, same way we did before, and I _made_ it.” The extra emphasis put him in a fit of wheezing, and the nurse came in to make sure he was alright.

As kids, it was our duty to investigate the haunted cave. Daring each other to climb up first, we stood in the lake, watching the water plunge the long way down and smash into the rocks. The frothy spray churned and ground at them like a witch preparing a potion.

Charlie started up first. Hand over foot, his nimble frame made the climb look easy. But for my awkward and clunky limbs, the injurious staircase posed a more challenging puzzle. Water rushed over my bare feet, fighting to dislodge my stance. There were no roots or brush to grab on to; the only thing that grew was a slick carpet of moss which seemed to repel my feet, urging me to slip and fall into the snarling rocks below.

Charlie stood at the base of the falls with the water pounding down onto his shoulders. He reached out his hand, but the ghost of Old Rotham’s Lake had me. My foot lost its purchase, and I fell backwards into the water.

When Charlie’s coughing calmed and the nurse was satisfied, she left us alone again.

“What did you find in there, Charlie?”

“I found her,” he whispered. “The ghost, her skeleton, was there, and she had carved a message into the stone.”

“What did it say?”

“It said,” he closed his eyes, remembering:

> _Beware, those who come near, I fight for my freedom and for my life._
> _Show your face as a friend, or be filled with regret. I never give up and I never forget._

When I fell off the rocks that day, I had broken my leg. Charlie happened to be borrowing a friend’s cellphone, and called an ambulance. We rode to the hospital, side-by-side, and there, I was struck with the thought: _Was I cursed now? What if it really_ is _haunted?_

Our parents, rushing to the hospital to meet us, died when their car slid and fell off the side of the mountain where we lived.

Had I been cursed this whole time? Did this story explain my broken leg, our parents, my ex-wife’s miscarriage, and the fact that my business had just gone bankrupt for the fourth time? Charlie had been struck with no such misfortunes. In fact his life had seemed perfect, except for the cancer, but shouldn’t he have been cursed as well?

_Show your face as a friend..._

“Charlie, what did she mean by _as a friend?_”

I could see his eyes growing heavy. His weakening breaths were spaced further apart.

“Come on Charlie, I’m right here.” I would have grabbed and held his hands, except they were still buried under the covers. Instead I put my arm around his chest and hugged him tight, willing his breath to continue.

He came around again and spoke: “I found a journal in the cave. The husband used to carry a salt shaker. Was a special one his brother made him. He would sneak salt from the cellar because their meals were so bland.” Finally Charlie’s arm emerged from under the covers holding the shaker. The glass was warped, but still intact, and the metal cap was covered in orange-brown rust. The fine white crystals were packed tightly inside, unmoving. “I found it at the bottom of the lake,” he said. “Take it. It will keep you safe.”

I saw Charlie’s chest sink as he extended his hand with the old salt shaker. His arm dropped to the side of the bed, and as the muscles in his hand slackened, the shaker fell to the cold tiled floor. The white grains of salt shone blue in the light of the full moon, scattered like broken dreams among bits of glass across the tile. The nurse rushed in.

“He’s dead,” I said.

She stared aghast at the floor. “What happened?”

“The ghost of Old Rotham’s Sawmill finally got him,” I said. “I suppose I’m probably next.” I stood up, and as I walked out I put my arm on her shoulder. “You’ll see me again soon, I suspect.”
