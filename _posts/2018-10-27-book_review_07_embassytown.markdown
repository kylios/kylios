---
layout: post
title: Embassytown, by China Miéville
date: Oct 27, 2018
categories:
  - writing
  - book_reviews
description: |
  My book review of China Miéville's novel: Embassytown
introduction: |
  Updated from my [review on Goodreads](https://www.goodreads.com/book/show/11382940-embassytown) (originally June 25, 2018)
---

Final verdict: 4 / 5 stars.

"Before the humans arrived, we didn't speak," the Ariekei tried to say. They couldn't say it because it was a lie, and the Ariekei can't lie. They communicate using Language, which humans can hear and understand, but which only those aliens and the Ambassadors can speak, for it's not the sounds of Language carry meaning, but the feeling and thoughts that are expressed through its utterance. The human Ambassadors, modified and trained since birth, give humanity a voice for these mystical aliens - also known as The Hosts - enabling trade, an exchange of technologies and customs, and safe harbor on this toxic backwater planet. But could it really be worth all this trouble just to trade for their biotech? Arieka is far, the shipping routes are long and difficult, and ships don't come around _that_ often.

Avice Benner Cho grows up on Arieka. She is popular among the other children, the Ambassadors, and the Staff of Embassytown. At one point in her youth, a strange thing happens to her: she is asked to be the subject in an Arieki ritual to encode her into their Language as a simile; she became _the girl who was hurt in darkness and ate what was given her_. She leaves confused, but no worse off. As years pass by, she forgets about this mysterious event and eventually decides to become an immerser, a starship pilot, traveling across the galaxy making shipments and having adventures, learning more about their civilization that their sheltered city can provide. It was her latest husband, Scile, a linguist who she meets on her travels, whose interest finally brings her back to Embassytown, where she witnesses and plays part in the transformation of The Ariekei and the town itself.

_Embassytown_ is not an overly long book, but it spans decades worth of time, or a couple hundred kilohours, as they might say in Embassytown. With good pacing, we learn of some of the driving history in Embassytown, and the events and customs which shaped the heroine Avice. But she is not a heroine, really. Upon returning to Embassytown, she becomes a _floaker_, using her savings (it's hard to spend your money living onboard starships for kilohours at a time) to practically do nothing. She attends parties, becomes a regular at the hippest cafes and bars, and hangs out with her android friend Ehrsul. But when her husband starts to stir up trouble among the Ariekei and the staff, she finds that she must choose a side, and it may not necessarily align with his. It is here in the second half of the story that the stakes increase and the action elevates, and it became impossible for me to put this book down.

Miéville gives away just enough details to let the reader imagine a world where these events may plausibly occur. The characters and the setting are shown vividly in all their alien complexity, yet there's an unsettling nature to everything that's created by the details that Miéville chooses to withhold. This isn't a science fiction story, really. In a way, this book evokes _Ursula K. LeGuin_ in that it builds a world that allows it to convey its ideas. As events unfold, the true conflict is revealed, and it's not about the Ariekei or Language or Avice or The Ambassadors. It's about understanding one another despite the different ways we think. It's about accepting change, and guiding it into place as the new normal.

This story will probably make you think about language and thought. It certainly did for me. But it will also make you think about evolution and change, and if the Ariekei in all their mysteriously advanced ways still have so much room to evolve, then perhaps us humans do as well.
