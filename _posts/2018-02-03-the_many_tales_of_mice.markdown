---
layout: "post"
title: "The Many Tales of Mice"
date: "Feb 3rd, 2018"
categories:
  - writing
  - stories
description: |
  An unlikely band of heros discover truths about themselves as they fight an
  evil wizard for freedom in their land.
introduction: |
  I wrote this story for [NYC Midnight's](http://www.nycmidnight.com/)
  [Short Story Competition](http://www.nycmidnight.com/Competitions/SSC/Challenge.htm)
  and won an honorable mention. I had a tough time with this one, but I'm
  reasonably happy with the way it turned out. The prompt was to write a fairy
  tale about cloning, featuring a nihilist.
---

She was a small mouse, a happy mouse. Quick and nimble, she darted around the land, patiently awaiting the packages left by The Wizard. He dropped one every day, and though she had to climb treacherous mountains or brave ferocious rivers to reach them, their contents never disappointed her, for they contained assortments of cheeses, peanut butter, strawberries, and all the most delicious of delicacies she could ever hope for.

On this particular morning, she felt it plop down off in the distance, and a faint smell wafted her way. Pointing her nose, off she ran, through the woods, across a creek, past the cows grazing freely on the hillside. As the wind picked up and sifted its way through the tall blades of grass, the smell penetrated her nose, and she gave in, letting it control her.

She reached a field of boulders, an array of stones so vast and rocky and complex, like a maze that she did not recognize. Her dutiful legs scurried and carried her forwards, improvising their way around the cracks and jagged edges as the strengthening smell encouraged her every step. _It’s just around this next corner_, she thought, when Bam! She smacked head first into another mouse!

"Oh! I’m terribly sorry!" she said suddenly. "I didn’t see you there."

The mouse had been thrown onto its side. It’s fur was disheveled, and specks of brown dirt covered the mouse’s sleek white coating. It rolled itself back onto it feet, but kept its head bowed low. "Ow, that hurt."

"Sorry," said the little mouse, and then, "hey! I don’t see many other mice around here. What’s your name?"

"I don’t have one. Does it matter?"

"Sure it does! Now that there are two of us, we have to have something to call each other."

"Why?"

"So that nobody gets confused," she said.

The mouse thought for a second. "Ok, call me Bruce. And what’s your name?"

"Sally." Her name was Sally, she decided. "What are you doing here?" Bruce had been sitting lazily next to a rock in the sun; didn’t he smell the package nearby?

"Same as I do every day," he said. "Nothing."

"Nothing?"

"Nothing!"

"But why?"

"Why not?" he said. "It’s the same thing around here every day. I don’t see any need to make a fuss about it."

"But don’t you want to come see what’s in this package with me?" she asked him. The smell grew stronger and stronger, and she could hardly resist its temptation. Bruce was unmoving and uncaring.

As she moved away, Bruce warned her, "I wouldn’t touch it if I were you," but she was hardly listening. His voice grew distant as she continued her search, and he didn’t come after her. She wondered why he would say such a thing, but the thought was fleeting as the promise of a delicious snack took control.

She reached the package, finally. As she chewed through the brown paper wrapping, the scent grew unbearably strong. A little bit of apple, some strawberry, and maybe… popcorn!? She chewed faster and faster, spewing little bits of brown paper everywhere until her nose finally broke through. The inside of the package glowed blue and purple and let out a faint humming sound. She sniffed around inside and her nose touched something smooth and cold, and then ZAP! Her body went stiff and she couldn’t move and her spine turned to fire and then everything went black.

***

When she woke up, it was dark outside. She felt the cold wind on her ears, her whiskers, her bones, and she had no memory of what happened. Where was she? Then she rolled over and saw the package, the brown cardboard with a large hole dug through it, singed black like a rotting stump. The blue purple glow was gone.

She called for Bruce.

The mouse was sitting against a rock with his eyes closed. A small bead of drool hung from his upper tooth and she could hear his heavy breathing over the gusty wind. Upon hearing his name, he opened one eye, then a second, but he didn’t move from his position.

"You’re awake," he said matter-of-factly.

Sally was both terrified and cautious. "What happened?"

"You fell for his tricks," he said. "I tried to warn you, but you were gone."

She stared at him, bewildered. Her nose stung as if she had just scraped it against a sharp rock, and her joints ached. The cool wind chilled her bones. "Whose tricks?"

Bruce sighed, and then began to explain: The Wizard was not a benevolent giver of treats, he was an evil man who was growing a giant army to conquer the lands ruled by the rats to the south. He doesn’t recruit his soldiers, Bruce explained. "He _makes_ them."

"How can he make soldiers?" She feared she wasn’t thinking straight.

"He finds the strongest mice in the land, and he makes copies of them with his crystal ball. What you felt in that package was his orb taking a piece of your soul to copy. There must be something about you, because he only copies those he thinks are special."

"That’s impossible!" she cried. She didn’t want to believe it. She wanted to believe The Wizard was a good man who looked after their kingdom.

Bruce only shook his head. "It is not so. You should have listened... you would be happier not to know."

She was struck by a question: "Bruce, how do you know all this?"

"I am one of the mice he made," he said. "I’m a copy, a clone, and I escaped. But I am without a purpose now, and so might as well be dead."

***

They huddled together, bracing against the cold. Sally felt sad and hopeless, defeated. She didn’t want this evil ruling her land, but she didn’t know what to do about it.

There in the midst of darkness, she saw a creature standing above them. It stood tall on its hind legs, its ears and cheeks carved a menacing silhouette against the rising moon.

"Leave us alone squirrel!" she shouted. For squirrels were known to terrorize mice and steal their food.

"Shhh!" said the squirrel. "They’re coming! An army of mice! We need to get safe. We need to do something!"

Sally snapped awake, leaving Bruce dazed and curled up on the rocks. "Bruce, listen! We need to stop The Wizard!"

"It’s pointless," he said, rolling over. "There are too many of them. You’re better off running away."

But she had made up her mind. "Bruce we need your help, and you are a copy. He took something from you and from me, and we need to stop it!"

But Bruce was unmoved. Was he really content with his life this way?

"It’s the only way to bring purpose back to your life," she said finally.

That lit something up in him. His eyes opened fully, and finally he said, "Ok."

***

Under the moonlight, the three of them stood atop a hill and watched the army march through their land. Animals scattered left and right as each synchronized footstep shook the ground and swayed the trees, and they heard a chorus of chants.

"There are so many of them, what are we to do?" said Bruce.

"I take it all back," said the squirrel. "Maybe we should run with the others."

"No," said Sally. "I have a plan." They listened intently as she told them her plan.

***

The Wizard watched it unfold through his crystal ball. The magic glass had served him well and produced a perfect clone. Now it was time to put her to use.

"These lands will be mine," he cackled. "All mine! And I will rule them all, and they will all be the same, one copy and another, and nobody will defy me!" The Wizard swiveled his chair around to the back of his chambers. There, a small wooden door sat crooked on its hinges. An iron lock kept the door bolted shut, and with a wave of his hand, it cracked open. "Come out, my precious little one!"

The door opened slowly, and out tiptoed a little white mouse. Her whiskers were long and slender, and she said in a soft voice, "are you my father?"

"Yes, my dear. I am your father, for I have made you. Your name is Elsa, and you will do something for me."

"What is it, father?" She said, eager to please.

He began to explain: "We are at war, my dear. The rats have long terrorized our borders and stolen our food, and now it is time we wipe them all out once and for all. It will be bloody, but it is very important that we win."

"I hear you, father. We must defeat the rats."

"Our army is vast and wide," he continued. "All the strongest and most obedient mice in the land are here following my orders. But you, my dear, are the smartest. So I am asking you to lead them."

"I will lead them father. I will lead them to great victory."

"I know you will, my dear."

***

"Stop! You there!"

A trio of Bruce-clone soldiers shouted as Bruce stepped out of the woods holding Sally by the scruff of the neck.

"I’ve found a prisoner," he exclaimed. "There, in the woods. She was spying on us."

"Where is your armor and weapon?" One of the soldiers asked.

"I was attacked by a squirrel and snapped my spear," said Bruce. "The beast tore the armor right off me! Luckily I got him in the heart with the tip."

The three soldiers eyed them suspiciously. "A soldier never loses their weapon," he exclaimed. "Go report to the lieutenant. I’ll take care of the prisoner."

As the soldier reached for Sally, their friend the squirrel (who’s name they had learned was Billy) jumped out of the woods and chewed the soldier’s head clean off. Bruce grabbed his spear and stood in between the remaining soldiers and the defenseless Sally.

They had picked a spot in the woods far enough away from the rest of the army so that the soldiers could not call for help. Sally knew they would find some small band of soldiers away from the others, imbibing or otherwise slacking off, like in any army. The two remaining soldiers stood at the defensive, unsteadily and afraid. "You’ll surely be executed for this, traitor!" One of them shouted. Bruce lunged at him with the spear outstretched, and as the soldier moved to block his attack, Billy leapt up and gashed the soldier with his long razor sharp claws, sending him to the ground in an unmoving heap. The final soldier turned to flee, but Bruce brought the spear up, and with a strong steady arm he flung it straight and true, and pierced the soldier right in the spine. He dropped hard and fast onto the ground.

The two mice each donned the fallen soldiers’ armor, and they each grabbed a spear. Here they parted ways, and Sally crept through the woods towards the rear of the army where the newly appointed commander would be starting her command.

Sure enough, under the light of the moon and a thousand torches, she saw her clone bounding up in her bronze-cast steel armor covering silky white fur and riding atop a badger. She held the reins firmly and with ease, riding to command the army like it was her birthright.

***

Elsa sat atop her badger basking in the glory of her command. All these thousands of mice with their strength and their armor waited eagerly to do anything she asked. Tomorrow would be a big day, she thought, and as she analyzed her strategy for their invasion of the rats’ country, she noticed a commotion off in the distance. A squirrel was terrorizing her troops. "Get that creature!" she shouted. Suddenly, out from the woods came hordes of creatures: mice, rats, squirrels, sparrows. The action was starting early.

"Fall back! Fall back!" she shouted to her troops. They obeyed, but they never turned their backs on the enemy. With their spears pointed outward at the ready, they retreated into a tighter formation and stood at defense. What she didn’t notice, however, was the little white mouse that had bounded its way up her badger and tapped her on the shoulder.

"Recognize me?" said the little mouse. It had a spear, and it attempted to drive it into her, but Elsa parried it away, and they skirmished there atop the badger.

The badger reared up onto its hind legs, not happy about a fight breaking out on its back. The two equally matched mice were thrown to the ground where they fought for command over The Wizard’s army.

***

With all these creatures by his side, Bruce felt confident they would complete their mission. They quickly gained ground over the mice, but their progress slowed as the mouses’ formation became more tightly packed until they faced a wall of spears pointed their direction. Arrows flew up into the sky, and sparrows fell to the ground. One by one, the rats and the squirrels took spears to the chest and fell with a thud.

Soon the mouse army had them surrounded. They were down to a dozen or so creatures, all packed in, back-to-back against the horde of mice.

"Put down your weapons!" Shouted one of them.

So he did. Bruce lay his spear down, and the creatures who carried them laid their weapons down too. They had to surrender. What other choice did they have?

And then all the mice stopped and turned. Bruce heard the command: "Stop! This way!"

Attentively, the soldiers listened to the sound of their commander’s voice. They obeyed, and they retreated, back towards the land from where came. For they took Sally for their commander. She was leading them back to The Wizard’s castle.

***

By the time they were through, they had captured The Wizard with his very own army and sent him off to live with the rats. Without his crystal ball, he was completely powerless. Bruce learned that even though he was a copy, he could still live his life with a purpose. And Sally had to learn to forage for food again, now that The Wizard wasn’t dropping treats all over the land looking for clever mice to clone. She found that she and Bruce made a good pair with her brains and his braun. The other mice hadn’t quite figured out that she wasn’t really their commander, so they accepted this new pair as their benevolent rulers without question, as they all lived happily ever after.
