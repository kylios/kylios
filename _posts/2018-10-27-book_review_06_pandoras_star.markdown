---
layout: post
title: Pandora's Star, by Peter Hamilton
date: Oct 27, 2018
categories:
  - writing
  - book_reviews
description: |
  My book review of Peter Hamilton's novel: Pandora's Star
introduction: |
  Updated from my [review on Goodreads](https://www.goodreads.com/book/show/526307.Pandora_s_Star) (originally June 19, 2018)
---

Wow, there are a lot of neat ideas in this book, perhaps even too many. _Pandora's Star_ is full of aliens, politics, relationships, subversion, exploration, you name it. Countless plot threads held together by a single overarching narrative that spanned 1000 barely concluding pages made it a bit hard to keep everything in my head, as the reader is introduced to new characters every 50-100 pages all the way up until the end. Perspective shifts to new characters midway through each chapter with detailed lifelike descriptions of their city and planet, but then you ask: have I met this character already? Or did they just share a first name with a different character? After a while you start to ask: do I need to pay attention to this detail? It was hard to get a good grasp of everything that was going on and how it was woven together until at least halfway into the book. It's quite a commitment. This may or may not work for you.

_Pandora's Star_ uses a brilliant opening sequence to lay the foundation for a future in which humanity abandons spaceflight in favor of wormhole technology. Using these faster-than-light highways, humanity expands in a controlled and conservative way, growing the bulky civilization they call "The Commonwealth" while still ensuring sufficient access to resources and protection for all citizens. Hamilton does a decent job at crafting this future society, but he falls short in imagining the ramifications of a lot of other new technologies, like artificial sentience, aliens, rejuvenation, and mind uploading technology.

Perhaps some of my favorite moments in this book are when it explores the various alien species and takes the characters on adventures to other worlds. There are some really fun, creative moments here which add to the vastness and diversity of this universe.

My biggest criticism of the book are the sexist undertones. Many of the major female characters feel like they were written as men, but made to be female because hey, it's the 21st century, and women exist. The other female characters felt overly feminine, overly sensitive, and too attached to their men, who they had a single Prince Charming moment with. There are several places where the author's views on political correctness and progressivism come right out, and while I think it's more than acceptable to have dialogs on these topics in literature, I don't think Hamilton approaches them with any sort of grace or open-mindedness, but rather a reluctance and obligation to be in the 21st-century where we value equality and diversity.

This is definitely a book for fans of galaxy-spanning space opera scifi, and one where you should plan to sink some time and energy into. It is fun mainly for the characters, the events, the aliens, and some really really awesome action sequences, but it fell short for me on the overall length and pacing, and on the speculation side, where there were a lot of gaps. As there is a lot more story to tell, I certainly plan to pick up the sequel, Judas Unchained, but I am not rushing to grab it off the shelf.
