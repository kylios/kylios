---
layout: post
title: Homage to Catalonia, by George Orwell
date: Oct 27, 2018
categories:
  - writing
  - book_reviews
description: |
  My book review of George Orwell's Homage to Catalonia
introduction: |
  Updated from my [review on Goodreads](https://www.goodreads.com/book/show/25197896-homage-to-catalonia) (originally May 20, 2018)
---

George Orwell fought in the Militia during the Spanish Civil War on the side of the Anarchists. In a time when Fascism was beginning to pose a real threat to society, Spanish revolutionaries resisted and fought back in attempts to take control of their country, economy, and fate. Orwell's accounts of the war, from life in the trenches to the battles in the streets of Barcelona, gave a good sense of the raw, gritty, nature of the war. Idealists from many other nations and political parties clashed to create an interesting dynamic. Orwell attempts to explain the political landscape at the time, and falls a little short, as these chapters are densely packed with names of political parties (as similar-sounding acronyms) and poorly organized descriptions of events, that it was difficult to really understand what started the war and what motivated all the sides. Despite that, the book gave a good look at what it was like to be there and what some of the ideologies and political forces were at the time.
