---
layout: post
title: Sorting Laundry, a Study of Algorithms
date: April 15th, 2016
categories:
  - writing
  - stories
description: |
  Dissect the algorithm behind efficiently sorting and putting away laundry.
introduction: |
  I was sorting laundry one afternoon - a prime time for thinking - and I began
  to take notice of the particular patterns and routines that I realized I had
  optimized for efficiency. It started to feel like an algorithm. So here it is,
  an algorithm for efficient laundry sorting.
---

Sorting Laundry is like an algorithm, a sequence of instructions. You could run it on a computer, almost.

Let's take a look:

You've brought your laundry bag full of unsorted clothes into your room from the laundry room. You need to fold each article of clothing and place it in the proper drawer. You have a drawer for underwear and socks. One for t-shirts and undershirts. A drawer for heavier shirts with sleeves. A drawer for pants, and finally a drawer for sweaters, sweatpants, and pajamas.  That's five drawers total.

Now you can start out by picking the first article from the top of the bag and placing it in the correct drawer. The procedure might look something like this:

* Place hand in bag, remove the top item
* Identify the item, fold it
* Open the correct drawer for that item
* Place the item in the drawer. Sometimes you have to reshuffle some things around to make space.
* Close the drawer
* Repeat

How many items of clothing did you just wash? This process seems tedious since you will have to open and close a drawer for each article of clothing. How can this process be sped up?

Since we have to extract and fold each article of clothing anyway, we can't really optimize that process. Each article needs to be placed in its correct drawer. However, there are only five drawers, and you might have fifty articles of clothing to sort, or more.  If you have some easy-to-access space around, you can minimize the number of times you need to open and close drawers, at the small cost of five piles worth of space. Maybe three square feet or so.

So now the new process might look something like this:

* Reach hand in bag and extract the first item
* Fold it. Identify in which drawer it belongs.
* Is there a pile for this article of clothing? No? Then make one. Yes? Add it to the pile.
* Keep this up until the bag is empty.
* Now that the bag is empty and you have five sorted piles of clothes in front of you, one-by-one you can open the correct drawer and place the pile into that drawer, shuffling things around to make room.

See, didn't that save some time?  Instead of opening and closing drawers about fifty times - the very approximate number of clothing articles you might have - you're probably only opening and closing one drawer per pile, which is five.  But wait, there's a little problem with this procedure. What about socks? "Folding" socks is really more like identifying its twin and finding them together. How can you do that while only extracting, folding, and sorting each item at a time? You'll need a special case.

Let's modify the procedure a little bit. When you extract a sock from the bag, instead of immediately placing it into the sock and underwear pile, place it in a temporary sock pile instead. Yes - you need a little more surface space, but it's worth it. Now you have six piles since your sock and underwear pile is two separate piles, one for underwear and one for unmatched socks. Once all the clothes are sorted into piles, you can go through the socks one by one and match them, placing the pairs in the sock and underwear pile.

You probably get the point, but just for completeness, here's what that algorithm may look like:

* Reach hand in bag and extract the first item
* Identify it

    * If it's a sock, place it in the temporary sock pile
    * If it's not a sock, fold it and place it in the proper pile

        * Is there a pile for this article of clothing? No? Then make one. Yes? Add it to the pile.

* Keep this up until the bag is empty.
* Check out your sock pile; for each sock in there, do this:

    * Find its pair.  If you can't, just throw the sock back in the pile and move on to the next one
    * Once you've found its pair, fold them together and place them in your more permanent socks and underwear pile.

* Now that you have five sorted piles of clothes in front of you, one-by-one you can open the correct drawer and place the pile into that drawer, shuffling things around to make room.

So that's the very basics of it.  Algorithms are no more than sets of instructions, typically optimized for speed or storage space.  They tell a system, not necessarily, but usually, a computer, what to do and how to do it.

## Getting Deeper

So you did pretty well optimizing that algorithm for speed, and compromised slightly on storage space, but most people can spare a few square feet, on their bed or on a desk, in order to sort their laundry.  You can even get fancy and sort it in the laundry room, then transport it to your room where you store it away in your dresser.  There's actually a number of optimizations you could do, but you've got to get a little deeper.

Let's think about the optimizations I mentioned earlier: speed and storage space.  How are you applying these things to your algorithm?

Speed was considered the first time you decided to sort your clothes into piles.  Remember, the point is to minimize the number of times the dresser drawers are opened and closed, because it takes time to do all that.

Now how does this algorithm optimize on storage space?  Well, I'm not sure it does.  However, there is a trade-off that is made fairly in my opinion.  Sure, the algorithm makes little attempt to save on storage space, but that's because it doesn't need to.  If three or four square feet of space is hard to come by, then you're in a rare situation.  The overall value of the algorithm comes from the savings in speed, but is hardly impacted by the storage space required.
