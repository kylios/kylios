---
layout: post
title: Welcome to School
date: Oct 21, 2018
categories:
  - writing
  - stories
description: |
  What starts as a simple trade school develops into a heated competition.
introduction: |
  This was my entry for the second round of [NYCMidnight's 2018 Flash Fiction Competition](http://nycmidnight.com/Competitions/FFC/Challenge.htm). They're still judging the results, so it will be another month or two before I find out how I did. I enjoyed this prompt, although I had a very difficult time this go-round. I found myself busy most of Saturday, but jotted down notes whenever I could. By Sunday morning, I thought I had a good plot and story skeleton, but when I sat down to write, nothing came out. Finally I got back into the flow in the late afternoon, and I submitted the story with maybe a minute to spare. I wish that I'd had more time to revise it, but overall I don't think the story is that bad. The prompt was to write a comedy, set in a training facility, and incorporate a leaf blower. So that's what I did...

  **Update**: I scored 7 points for this story, which means I took 9th place out of the 30 contestents in my group. Not bad!
---

“Welcome to school,” said a burly man in a greasy tank-top. “Name’s Bruce. If you need a wrench turned, I’m your guy.”

“You ready to join the best of the best?” a woman held an oil-soaked rag. Between a toothpick and gritted teeth, she said, “we’ll straighten you right up. I’m Dominique.”

“Welcome to the team.” Stacy, instructor and team lead, greeted me as I arrived and led me through the nondescript gray building. The Appliance Servicing School, or A.S.S. for short (because why waste time on stuff that doesn’t matter, she said), had many departments: home appliance, industrial, lawn and yard, kitchen, dorm room, personal pleasure… you name it. There were appliances I never imagined, like breakfast-in-bed rice cookers and ceiling vacuum cleaners, but the one thing they all had in common was that A.S.S. could repair them, or train somebody to do so.

My new teammates introduced me to the concepts of doors and dials, detergent and water, screws, nuts, washers, and wrenches. The team specialized in dishwashers. If I graduated, I would become a professional dishwasher repair tech.

“You got all that, Eric?” said Dominique as she finished describing the parts of the dishwasher. Overwhelmed, I struggled to produce an affirmative reply. She eyed me suspiciously. “You better be ready on Friday. The annual repair-off is coming up, and there’s no fucking way we’re losing again.”



On Tuesday, Bruce showed me how to replace a door. “You apply the loctite, keeps the screws in there while the machine is shaking.” To demonstrate, he lifted a dishwasher in his tree-trunk arms and shook it wildly. The door remained firmly latched. “Next one’s yours,” he said, pointing to a doorless dishwasher.

As I squeezed the loctite into the threads of a screw, I heard a shout behind me. A lanky man with greasy black hair stood holding a leaf blower between his legs in an obscene gesture.

“You’re going down, dishwasher. Down to drown!” he called at me. “Be prepared to get blown away!” he thrust his hips back and forth before pulling the cord to start the engine. It roared to life. He cackled as he galloped away.



Over the next few days, I learned how to replace and refurbish nearly every component of the dishwasher, but the door was the hardest. “Without a good door, you’ll end up washing your whole kitchen,” said Dominique. “The best-of-the-best won’t allow anything less than a good seal. You want to be best-of-the-best, right?”

Meanwhile Bruce demonstrated how to quickly swap out a roller assembly and a spinning arm. “Gotta do this in under a minute to win the competition.”

But most importantly, Stacy coached us on teamwork. As the competition loomed over us, she stressed the importance of good communication, trust, and knowing what the fuck everybody was doing. “You can’t perform a successful hand-off if your teammates don’t know what you’re responsible for.”

Walking back to my car after my fourth day of school, the greasy-haired leaf blower sauntered over. “So, how’s school? Excuse me, I haven’t even introduced myself.” He reached out a hand, then retreated in a classic handshake fake-out. “Make sure to bring your snorkel and life jacket tomorrow. We don’t want anyone to drown!”



I came in early Friday and in my head I ran through every step of building a dishwasher. We had one hour, and I was determined to be ready.

Dominique and Bruce showed up and fell into their respective routines. Fifteen minutes before the start, Stacy still hadn’t showed.

“What do we do?” said Bruce.

“She’ll show,” said Dominique, whispering assurances under her breath.

The A.S.S. coordinator Larry called us out to the parking lot, which was home to piles of broken appliances, stacked up taller than the facility. Bruce, Dominique, and I exchanged glances as we lined up facing the piles.

“Using pieces from the pile, each team must build their appliance in under an hour! It must be sellable!” Larry explained to the group of excited students. “You’ll compete head-to-head with one team. Best team wins!”

Oh God, no, I thought as I faced our opponents. Four leaf blowers grinned at us, muscles bulging from their matching green and brown suits.

“You ready to be blown away? Or you gonna drown instead?”

“Shut up, leaf blower,” said Bruce. “You ain’t good for shit.”

“Come on guys,” I said. “We can do this.”

Larry counted down the start, and soon we began. Bruce found a door and roller from the pile, while Dominique began greasing up a motor. I applied loctite to screws and arranged all the necessary hardware.

Dominique installed the motor and spinning arm, then Bruce to installed the door. The leaf blowers were well ahead of us; they were already running the engine.

“Suckers,” said the greasy-haired one as he blew trash in our direction.

Bruce was assembling the dial and reprogramming the buzzer. “Dominique,” I said, buying time. “Can you get to the water supply?” She nodded at me, understanding. “I want full blast,” I said. Quickly disconnecting the hose from the sprayers in the spinning arm, I popped open the door.

“Prepare to drown, leaf blowers!” Right on cue, Dominique cranked on the water to full blast, and a jet of water blasted out of the open door at the opposing team. The engine puttered to a stop as it drowned in the spray.

“Alright boss!” Bruce held up the buzzer and dial, and quickly installed it into the dishwasher. I reconnected the hose, tested the door’s seal, and then declared, “done!”

“And we have our winner!” shouted Larry.

Just then Stacy appeared. “Did I miss it!? I overslept!”
