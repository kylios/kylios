---
layout: post
title: "Speculative Fiction: The Answer to the Scifi vs Fantasy Debate?"
date: Oct 21, 2018
categories:
  - writing
  - blog
description: |
  Why I like the term "speculative fiction" rather than "science fiction" or "fantasy."
---

Science Fiction Examples:
- Stranger in a Strange Land
- The Left Hand of Darkness
- The Handmaid's Tale
- Babel-17, Nova
- Station Eleven
- Annihilation

Fantasy Examples:
- The Lord of the Rings
- American Gods
- Harry Potter
- A Song of Ice and Fire
- The Broken Earth
- The Goblin Emperor


Horror Examples:
- The Stand
- Tommyknockers
- The Haunting of Hill House
- Annihilation
- 

Weird Fiction
- China Mieville
- Jeff Vandermeer
- 


Outline:

- Introduction
	- My history with science fiction and fantasy
	- Why I read science fiction
	- Why I read fantasy
	- Why I read weird fiction and horror
	- Assertion: the genre tags are wrogn
- 


My history with science fiction and fantasy goes way back, further than my first copy of Ender's Game, further back than my introduction to Star Wars when it came out again in theaters in 1997. I remember, before that, watching E.T, The Dark Crystal, and some weird David Bowie film. I would watch Totoro on repeat when I was like 5 or 6.

But I also struggled through The Lord of the Rings, even though I love the movies. I never finished Harry Potter. I was always drawn more to the science fiction end of the spectrum - which I'll talk about shortly - but not even the "hard scifi," as much. These genres can get pretty nuanced, and I'll talk about them more as well, but you can think of "hard scifi" as the these-are-the-facts type writing: hard worldbuilding, the science comes first, you usually learn something. They biggest problem with these is that the characters can fall flat, the world can get tricky. I was always drawn toward more character-centric stories. When I saw Ender's Game on the shelf in my 4th grade bookfair, I knew I had to buy a copy. We didn't have much money: we could only afford a few things, but after the second or third trip into the library and reading the back of that book, I just knew I had to get it. Something about the triangular spaceship zooming away from some vast alien structure. Or at least I thought it looked alien. Ender's Game introduced me to this kind of character-driven science fiction, while still satisfying that spaceships-and-aliens craving that I had after seeing Star Wars.

I learned a lot from Ender's Game, though. Mostly I learned to "love your enemy." I think this is important. Even the people we don't agree with, we should try to understand, and see things from their perspective. This is what Ender did. But he did more than that. And he would defeat them anyway.

---

Speculative Fiction: What about Science Fiction, Fantasy?



---

Science fiction consumes a good deal of my time spent reading. I used to say that I liked the spaceships and the technology, and the speculation that comes with stories of alien encounters and virtual reality worlds. But as I read more and more, and branch out into sub-genres that are science fiction in name only, I realize that I read for a different reason.  Lately I've been trying to read more fantasy, but generally the genre appeals to me less. Trying to figure out why, I make an effort to read more of the genre, and I realize that it's just as entertaining and thought-provoking as its more grounded-in-science cousin. But is science fiction really defined this way, as being "grounded in science?" What about horror and weird fiction? What defines their boundaries with the fantasy genre? After examining the similarities and differences of these genres, I find that the lines blur quite substantially, and so I prefer the term _speculative fiction_, and here's why:

In [this article published by Gizmodo](https://io9.gizmodo.com/5650396/margaret-atwood-and-ursula-k-le-guin-debate-science-fiction-vs-realism), Ursula K. LeGuin debates Margaret Atwood on the definitions of these genres. They conclude the breakdown as one of science fiction could someday happen, while fantasy could never happen. Speculative fiction could happen today. Of course all this is framed by technology. They throw out examples like wormholes, credit cards, DNA identification, and spaceships. If the presence of wormholes makes a story science fiction (because we can't yet do that), then the presence of DNA identification would land the story on speculative fiction shelf. So what about _Gattaca_? This is widely held science fiction, but should it be speculative fiction instead?

My view is that defining these genres by the stories' inclusion of technology is limiting. Let's examine some fantasy for a moment. Sure, something like _The Lord of the Rings_ is pure straight-up swords-and-wizards fantasy, no questions asked. What about _Harry Potter_? Sure, witches and wizards wave their magic wands around to cast spells, goblins and giants exist, and all sorts of strange unexplainable things occur. But in order for the story to work well, J.K. Rowling had to set in place a set of rules, and one that could not be broken: one's ability to use magic is inherited; muggles cannot wield it. With this foundational rule, she is able to create a character in a difficult situation, and a highly interesting world all around him. We are able to examine the repurcussions when this boy's "gift" is finally exposed, the animosity that the Dursleys feel towards him, and how these things shape the person he becomes.

Fantasy is becoming ever more enshrined in systems of rules, especially as worldbuilding becomes more popular. Tor.com [discusses](https://www.tor.com/2017/08/24/epic-fantasy-and-breaking-the-rules-of-infrastructure-in-the-interest-of-speed/) much of the infrastructure that sets the stakes within Westeros in _A Song of Ice and Fire_ (_Game of Thrones_). George R. R. Martin builds a vast and complex world, and an equally complex system of rules that govern it. Many of these rules have nothing to do with magic, but instead concern transportation routes, economics, centers of power. The story is now forced to examine deep complexities and nuance in human behavior as it navigates the deep complexities of this fantastic world. Contrasted with _The Lord of the Rings_, which is simply a story of good versus evil, _ASoIaF_ feels like a textbook next to a fairy tale.

Suppose that the magic was removed from the _A Song of Ice and Fire_ story. Would it still be considered fantasy? Ken Liu did this with his _Dandilion Dynasty_ series, which is set in a fictional Asia and concerns empires and dynasties, surrounded by complex worldbuilding. This is set firmly on the _fantasy_ shelf, but without a system of magic, what is it really? Some folks are throwing around the genre tag _silk punk_, but to me that feels like as sub-genre just as _space opera_ is to _science fiction_. So what makes it _fantasy?_


Thinking about _why_ I read science fiction (and also what I enjoy about the fantasy I've read), I realized that the true importance of these genres is what they allow us to discover about humanity by thrusting people into strange and fantastical situations. We enter these worlds to escape our own, but we come out with the ability to examine our own world through new lenses. Whether it be a story about the last survivor on a far-away space station, or a small town in a post-apocalyptic landscape, or even the story of a young boy who learns he has the power to weild magic, if only he studies the right things, we can learn something important about ourselves in a way that wouldn't be possible if we were restricted to real-world settings and situations. After all, realism is just that - _real_. We hear those stories on the news, on Facebook, and at the bar. Why read about them in fiction too?

I have always used science fiction and fantasy to escape the hardships of the real world. These stories often tell of characters whose hardships are much harder than mine, and I guess that gives me some solace. [we escape the real world with unreal stories, but what we come away with is a better understanding of the real world - cite examples]




http://nkjemisin.com/2012/06/but-but-but-why-does-magic-have-to-make-sense/


 but as Arthur C. Clark said: "any sufficiently advanced technology is indistinguishable with magic." 


What about fantasy with well-built magical rule systems? Could this be classified as "science, where the spells are technology?"

What about science fiction that has no advanced science in it - Station Eleven, The Handmaid's Tale?
Why are aliens science fiction? Consider District-9, Arrival (Story of Your Life).
Cross-genre stories, where horror meets scifi, or fantasy, like Alien, Stephen King (Tommyknockers, The Stand, etc), zombie stories (which could be horror, horror/fantasy, horror/scifi, or just scifi).
What about weird fiction? Is it a blend of fantasy, scifi, horror, or what?

So the real dichotomy is: what makes us speculate, and what does not? It's hard to imagine a creative and unique world such as scifi or fantasy without calling for some speculation. Therefore, I think the right and useful term is "speculative fiction."





our brains have always outraced our hearts, our science charges ahead, our souls lag behind.
- Lee Adama, final episode of Battlestar Galactica