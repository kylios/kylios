---
layout: post
title: This Census-Taker, by China Miéville
date: Oct 27, 2018
categories:
  - writing
  - book_reviews
description: |
  My book review of China Miéville's novella: This Census-Taker
introduction: |
  Updated from my [review on Goodreads](https://www.goodreads.com/book/show/25489159-this-census-taker) (originally January 6th, 2018).
---

I read this book in one sitting while on a plane. It's 200 pages, but the pages are short.

Immediately I fell in love with Miéville's lyrical writing style. Creepy, unsettling, and complex, I was captivated from the very beginning by the way he crafts his sentences and ideas. This was my first Miéville, however, and as I read, I realized that perhaps this book was not the right introduction, as some other reviewers appropriately suggest. The writing, while beautiful, is difficult and layered. There are tense changes and perspective changes, and if you're not ready for them (like I was not), then their meaning will pass you by. But Miéville does everything for a reason, I've learned, so if you're already a fan of the author, then you'll probably get a lot more out of this book than I did.

At the very least, I got an introduction to China Miéville, which is great because I've highly enjoyed some of his other work. Otherwise, I got a complex and fascinating book that raised more questions than it answered. I will definitely be revisiting _This Census-Taker_ at some point.