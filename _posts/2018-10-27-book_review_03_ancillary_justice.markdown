---
layout: post
title: Ancillary Justice, by Ann Leckie
date: Oct 27, 2018
categories:
  - writing
  - book_reviews
description: |
  My book review of Ann Leckie's novel: Ancillary Justice
introduction: |
  Updated from my [review on Goodreads](https://www.goodreads.com/book/show/17333324-ancillary-justice) (originally Feb 13, 2018)
---

People have been calling Ancillary Justice a "Space Opera," but I'm not sure I'd classify it that way. When I think of Space Operas, I think of space ships and space battles, zero gravity, flying among the stars and planets. Ancillary Justice features space ships prominently, but in this story, they are characters, not things.

The story follows two timelines: in the present, our main character Breq is on a mission of her own making on a planet that is not yet part of the Radch Empire. In the second timeline - the past - we learn that Breq is actually a space ship called _The Justice of Toren_, and we learn of the events which lead her to embark on this mission. This story has a very unique take on these ships. Each space ship is comprised of a hivemind-like AI system, spanning the entire ship and a number of Ancillaries: human bodies purged of their original consciousnesses and loaded with a version of the ship's AI and then networked together. Ships can think and experience things as their ancillaries, and carry out missions on the surface of planets or space stations, interact with people, and other things that humans do. I found this concept pretty intriguing, and it provides some excellent narrative opportunities to play with first person perspective, which the author Ann Leckie executes flawlessly. The ancillary Breq needs to pass as human, begging questions of identity. What does it mean to be a person?

The Radch Empire is also fascinating, and Leckie has done a fair bit of worldbuilding - not too little and not too much - to give life to this empire. In the story, "the empire" is synonymous with civilization, and thus any member of the empire is considered "civilized." The Radch Empire conquers and assimilates rogue planets and systems, turning many of their citizens into ancillaries, which poses certain questions of morality. We get to see this world from the perspective of a planet outside the empire, a planet that has recently been assimilated, and a space station that is well established within the wealthy depths of the empire.

Another very interesting part of this book is the use of gendered pronouns. The Radchaai language lacks gendered pronouns for most things, and Leckie has decided to use the feminine pronouns much like _The Left Hand of Darkness_ uses masculine pronouns for gender-neutral language. The effect is interesting, as I began to assume that all characters were female until some clue revealed their true gender. This strengthens the theme of identity that the novel embraces, but otherwise I felt like these pronouns (or lack of) did very little for the plot, and actually added an air of "preachiness" to the writing. I didn't really mind, but the fact that Leckie decided not to use the gender-neutral pronouns "they/their/them" did raise some questions.

Overall, I give this book a strong 4/5 stars. I'll reiterate that I actually thought the use of gendered pronouns added a fun and interesting element to reading this book, I just wish it had served the plot a bit better.
