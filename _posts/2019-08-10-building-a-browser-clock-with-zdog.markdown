---
layout: post
title: Building a Browser Clock with Zdog
date: August 10, 2019
categories:
  - projects
  - programming
description: Using Zdog to make interactive pseudo-3D renderings
---

[https://kylios.gitlab.io/browser-clock/](https://kylios.gitlab.io/browser-clock/)

[Zdog](https://zzz.dog/) is a tiny javascript library than can "pseudo-render" 3D shapes. Rather than rendering the full volume, it's simply rendering a projection, which makes it quite fast and easy on your computer's resources. I wanted to play around with this, because if I ever make a game again (fingers crossed), then it would be fun to try something in 3D.

I like making clock programs. That was one of my first graphical java programs as well. They're simple and a little bit useful, and involve some trigonometry which I don't usually use in my profession or day-to-day life.
