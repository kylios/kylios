---
layout: post
title: Choosing Life in the Valley
date: May 26, 2018
categories:
  - writing
  - stories
description: |
  A harsh reality is buried underneath the utopian pretext of a post-
  revolutionary society, and a young person, tired of searching for
  meaning, seeks a permanent escape in the form of suicide.
introduction: |
  I was inspired to write this story after reading
  [Bolo'Bolo](https://www.goodreads.com/book/show/381554.Bolo_Bolo).
  The book describes how an anarchist society might deal with suicide,
  by not only allowing it, but making the means readily available by
  providing each member of the society with a poinonous vial they keep
  with them, should they need to "quit the game definitely and escape
  from [their] (improved, reformed) nightmare." In my story, suicide is
  not encouraged, but certainly not disallowed, and I thought it
  fascinating to examine how post-revolutionary anti-authoritarians
  might deal with this.

  I submitted this story to [Fireside Fiction](https://firesidefiction.com/)
  in April 2018. It was no surprise that the story was rejected, but it
  was fun anyway, and I'll likely try submitting future material to them.
  I highly recommend checking out that project if you enjoy speculative
  fiction and short stories!
---

The mountains enclosed our small village and the fields around it like a pair of hands carefully cupping a damaged chick found on the side of the road. We were isolated, but not alone, for the caravans came every month and left with grain, vegetables, and livestock, breaking the tenant of self-sufficiency that defined our existence. I didn’t know what our village received in return until the day the caravan saved my life.

The tall cabs rolled over the hilltops with their trailers in tow, escorted by pickup trucks and jeeps, people with guns. I watched them pull into our streets and park in front of the silos. They got out and shook hands, made small-talk, then were led to the commons. Was it fear, excitement, guilt, or shame that I felt the most? It didn’t matter; I ran to meet them.

***

The revolution happened when I was just an infant, said my parents. In a massively coordinated movement of subversion and resistance, they destroyed institutions of power, built communities, and stopped participating in the game of the world. By the decade’s end, human life was spread among a diaspora of small communities, borderless and efficient, happily living through cooperative trading and self-sufficiency.

I found collections of books and vids and spent hours alone, reading about the old world. “Why don’t you go out and play with the others?” said my parents. But I didn’t want to; I had nothing to offer. I preferred these solitary activities, and when they asked me to work I would go to the fields and monitor the machines.

I watched their temperature, oil and power levels, and made sure they didn’t jam up as they gathered our grains and watered our crops, enabling our lives of leisure. While my peers spent their time playing, exploring hobbies and passions, discovering each other, I stood alone in those fields thinking about how pointless it all was. What were we doing here besides living, working and playing, and why? Gone were the days where we built towers to the sky and flew to the moon, or strove for anything but to be happy and free. It felt like we’d lost something.

"You just haven't found your calling yet," my mom would say.

"Life is there to be enjoyed. Don't worry about anything else," said my dad.

They were forerunners of the revolution; they helped build this place. What was I but a disappointment?

Tired of the darkness, I decided to see the doctor. At first the medication only made it harder to feel. I needed something stronger that could make it all go away, end forever.

The village doctor looked puzzled.

"Well that's no solution," he said. "You want to fix your problem? Go build a car, travel the world, join a university. You're not doing yourself any good sticking around here if that's how you feel."

What was he talking about? I grew up here. This was my home, where people knew me. How could I leave?

I persisted and pleaded. I never asked for this and I didn't want to be here. Give me something to make the darkness come and free my spot on Earth for someone who wants it.

Eventually he said, "I can’t stop you from committing suicide. But you're valuable to this world... I won't help you."

I was helpless and angry, but the doctor spoke up again: "If you must, a colleague of mine will do it. She manages the hospital in the village over the mountain. Go with the next caravan and see her.”

When the day finally came, I said goodbye to my peers. They looked away, not knowing how to talk to someone who didn’t want to live. My parents pleaded with me, told me how great life was “if only I chose to participate.”

Rivers rolled down my mom’s cheeks. I wanted to fall apart and flow into the oceans the way they did.

As we departed, I sat in the bed of a pickup truck and waved back at them all. I said I would miss them, but I was relieved. My stomach turned with every twist in the road that led us over the mountains; the autumn wind whipped through my hair and the leaves glowed in their orange-yellow brilliance, and as we drove through the woods where the crisp air and the smell of dead leaves penetrated my senses, I saw our whole village all at once for the first time, from further away than I had ever been before.

***

I gained a family that night as we sat around the space heater singing, laughing, and storytelling together, bound by its warmth, long after the drivers retired to bed. The connection felt more real than anything, so when the bandits descended, puncturing the night with their bullets, they crushed my emotions like a fragile wing under a heavy tire. All the soldiers they trained and the escorts they dispatched couldn’t stop these raids. Protecting the food production came first, but my eyes were opened that night to the horrors that lie beyond our nest, the sacrifices made for our survival. As I emerged from my shelter into the ensuing silence, a woman gasped in her final breaths, instructed me to follow the road to her village and convene a council to end this tragedy once and for all.

I pushed aside my shame and my guilt, and I carried on towards their village.

In this world with no laws, outlaws aren’t just those who will kill to steal food from others. Without countries or taxes, it costs little to join society. You just have to participate. So today I lead groups of women and men from my village through these hills, on a mission to bring the outlaws in to our communities, give them jobs, food, a purpose, to let them participate in this world. There’s room for everyone here. No matter how hard we try, I keep coming back.
