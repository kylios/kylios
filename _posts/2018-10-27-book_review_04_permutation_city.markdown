---
layout: post
title: Permutation City, by Greg Egan
date: Oct 27, 2018
categories:
  - writing
  - book_reviews
description: |
  My book review of Greg Egan's novel: Permutation City
introduction: |
  Updated from my [review on Goodreads](https://www.goodreads.com/book/show/20344594-permutation-city) (originally April 29, 2018)
---

Greg Egan has a brilliant imagination and a strong understanding of the science behind his subject matter (or at least he is very good at faking it).

I had read enough reviews to come into this book knowing what to expect: a mind-blowingly cool idea with good execution and plausible science. For the most part, this book delivered. Permutation City explores life and consciousness in ways that could make you question your reality. If you're a fan of these "simulated universe" theories, then you should read this book.

_Permutation City_ demonstrates the challenges in simulating a universe by contrasting two different types of simulations created in a future world that's not too distant. He is pretty easy on the "info dumps" - that is to say, he doesn't spend a lot of text explaining the technical details, and he uses particular details and scenes to show what he's trying to convey. This works to some extent, but as a technical person, I was wishing that he would spend a little more time explaining the math and science behind his ideas.

Egan is also a technical person, and the writing methodically employed characters and situations that opened opportunities for plot. Many of the characters felt rather flat, although one of the characters had many chapters of development with very little contribution to plot. I kept wishing the narrative would return to a couple select characters where the real meat of the story was taking place.

Overall, Permutation City is a fascinating look at artificial reality, simulated universes, and the nature of perception. Despite its age, the science holds up quite well, as do many of his predictions of future society. Folks interested in the current debate over the simulated universe theory would do themselves a favor to read this novel for both a good bit of entertainment and a fairly plausible look at how this might realistically (or not) be achieved.
