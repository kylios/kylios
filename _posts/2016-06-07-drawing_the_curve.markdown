---
layout: post
title: Drawing the Curve
date: June 7, 2015
categories:
  - writing
  - stories
description: |
  Can you picture it?
introduction: |
  I dreamed this scenario. Drawing a curvy line in such a way that the reader
  could visualize it seemed like an interesting challenge, so I decided to try
  it out.
---

I pulled my chair back to a desk near the rear of the classroom.  I flopped my notebook down onto the surface and plummeted into my seat.  Our teacher had carved an intricate dance of loops and twists into the blackboard, reducing her stick of chalk to a bare stump.  The lines appeared less like scribbles and more like twists and knots of rope, turning and weaving in a slightly less-than-random pattern on the board in front of us.  Now your assignment, for the rest of the lesson, is to describe what you see in front of you, so that any reader may reconstruct this image exactly.  How does one translate a drawing of this sort into words?

And so I began:

Beginning from left to right, the line steeply descends like a roller coaster relieving the tension from a suspenseful ascent.  It abruptly reaches a valley and rises again, doubling back on itself for a short while before levelling out and continuing its rightward movement.  It drops quickly again, but this time continues its arc backwards into a loop, rising behind itself from the viewer's point of reference.  This is now followed by a long upward arc with its slope gradually decreasing until the line plateaus.  Another abrupt drop sends the line moving downward and leftwards in a deeper, broader loop which then hooks up in a forty-five degree angle, over the previous plateau, and rounds off into a peak.  From the peak, the line then drops straight down towards the ground.  The drop extends below and through the broad loop, such that if this were pulled tight like a rope, a knot would form.  A rounded ninety degree angle ends the drop and sends the line to the right once again.  The line begins to rise in a very gradual upward slope, but this is the start of an 'S'-like curve.  It doubles back on itself for a short stint, then extends to the right again in a longer upward slope.  The slope gradually decreases in yet the largest arc we have seen.  The arc rounds out and begins to fall once again, but this time far longer than any other drop.  The line ends in a large crescent, like the right side of a 'D', and terminates directly below the start of the 'S' at a distance nearly equal to the height of the 'S'.
