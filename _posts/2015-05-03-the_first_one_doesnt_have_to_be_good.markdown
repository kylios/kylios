---
layout: post
title: The First One Doesn't Have to Be Good
date: May 3rd, 2015
categories:
  - writing
  - stories
description: |
  Random thoughts on getting started in writing.
---

This neighborhood is fairly active at 10:30, despite the day being Sunday.  I am standing on the road next to the sidewalk, so my cigarette smoke won’t blow in the face of any passerby walking their dog or taking home their date.  All around, breaking the darkness, windows are illuminated by big screens playing Netflix, hogging my internet bandwidth.  When I get bored, and only once per day now, of course, I like to step outside and smoke a long fresh American Spirit, and think about what I’m working on, away from my desk and computer.

On this particular evening I happened to have read several clever stories by Andy Weir.  Author of The Martian.  Thinking I could try my hand at writing a clever short story, I stepped outside to think about something I could write about.

        _People_
                _Space_
                        _Science_
                                _Philosophy_


Themes that lack substance, lack experience.  There needs to be a story.

Among the distant sounds of busses, cars, and late-night pedestrians wandering, procrastinating the acceptance of the start of the work week, I faintly hear a rap on a radio.  It’s getting louder as it approaches, block by block.  A car stops at the intersection, _that’s not it_.  A bike is pulling up next to it, blaring a radio out of the backpack of a big black man wearing one of those big fuzzy coats and a beanie.  _This is racial profiling,_ I know.  It’s wrong, but we need to accept that it’s still there.  And work hard to be a good example to the next generation.

The bike is pulling into the intersection, slowly, meandering down the crosswalk.  He’s stopping, but just for a moment, now he’s turning to come down my street.  As he crosses the intersection and rides nearer to my cigarette and me, I have a brief flash.

It could be the drinks I had, or the cigarette, or the want of a good story, but my mind projected an alternate reality.

The man quickly gets off his bike and approaches me, forcefully drawing a gun out of his coat and extending it in the precise direction of my chest.  I raise my hands by my head, keeping sure to hold on to my butt.  As the man is drawing nearer, I try to remember what I learned so many years ago in karate training.  _Hands by the head where your opponent can see them.  If they draw near enough and are not keeping their cool, you can act defensive but execute a number of maneuvers to disarm and detain the attacker.  Got it.  I knew what I was going to do._

The man held his gun up to my head, only a meter away.  I said:

“what, are you trying to rob me?”

“Does it look like I’m trying to rob you?  Gimme your money!”

Then and there I regretted having brought my wallet out to smoke.  _Hey, you never know when you might need it._  If often doesn’t occur to me there are other risks associated with simple decisions.

I tried to think of something smart to say back at him, but all I could do was throw my butt into his face.  It startled him, but wasn’t enough.  I lunged up to his right side, grabbing the shoulder of his outstretched arm, and smacked the knuckles on his gun-wielding hand with the palm of my hand.  There is a special technique that I can’t understand except by doing it, but it worked, and the gun went skidding onto the pavement.  _Phew_, it didn’t discharge.

But now I’m in a conundrum.  He’s now fully prepared to kick the living shit out of me, and I’m far too small and weak to defend myself against this man.  I punch him in the face with my right hand.  I didn’t even think about doing that, wow.  I punch him again and grab the wrist that held the gun, and I twist it backwards as far as it can go.  I use all my force and shove him to the ground, applying enough pressure to his wrist to squash a watermelon.

I can’t think right now.  Thinking is usually my strength, now it is nothing but a weakness.  I resist the urge to think, and I bring out my phone, using the emergency call feature for the second time ever.

I have to press my boot against his head and curse at him to stay down.  _I’m calling the police,_ I scream at him.

The operator answers.  I tell her my name, address, and the situation.  I kick the man in the ribs because he’s getting out of my grip.  I tell her to come quick.  Well not _her_, but you know what I mean.

At this point the flash begins to fade.  The man rides by, bobbing to his music in his big fuzzy coat.  My mind is restless, unfocused.  _Why did I step out here?_

The story I want to tell, I have it, the first one doesn’t have to be any good...
