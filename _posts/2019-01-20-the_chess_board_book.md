---
layout: post
title: Binding the Chess Board Book
date: January, 2019
categories:
  - projects
  - bookbinding
description: Design binding notebook/sketchbook with a chess board cover
---

I bound this notebook in the beginning of 2019 to get more practice with sewing on tapes, sewing headbands, and making made endpapers.

<blockquote class="imgur-embed-pub" lang="en" data-id="a/UFxbdo3"  ><a href="//imgur.com/a/UFxbdo3">The Chess Board Book</a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>

Overall I'm relatively happy with this book. I learned some things, and the book is very usable. The ribbon marker adds a nice touch, and I love the chess board pattern on the cover. I use this book to take notes and make sketches during my Dungeons & Dragons campaigns, so I find the cover pattern to be quite appropriate.