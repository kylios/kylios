---
layout: post
title: The Last Stop Before the Sea
date: Sept 17th, 2017
categories:
  - writing
  - stories
description: |
  An obsessive search for a long lost grandfather leads to a startling
  conclusion.
introduction: |
  _The Last Stop Before the Sea_ is the second piece I wrote for
  [NYC Midnight's](http://www.nycmidnight.com/)
  [2017 Flash Fiction Competition](http://www.nycmidnight.com/Competitions/FFC/Challenge.htm).
  My prompt this time was to write a mystery which took place in a tobacco shop
  and somehow incorporated an urn.
---

The smell reminded me of him. Spicy, woody, musky, it smelled of age and experience, of deep wrinkles and a crackled voice, of a mustache more salty than pepper.

The tobacco shop carried everything. On one side wall, cigars were arranged like logs on slanted wood shelves, from Monte Cristo and Romeo y Julieta to the finest Cubans. On the other side, tins and bags of pipe tobacco proudly displayed their labels. They even carried some shisha from Egypt. He would have loved that, thought it was just so exotic.

I didn’t come there for any of that, though.

When I entered the store, the shopkeeper wasn’t anywhere in sight. A stout man sat in the back with a pipe stuck to his lip and a book in his hand. He let out a billow of smoke, never looking up.

Being there, I couldn’t push away the memories of my grandfather. He was a sailor who’d left over seven years ago, this time for good. He carried with him the smell of his beloved Indian tobacco, and as his pipe filled our home with its incense, he would tell of his adventures sailing around The Caribbean, through the Panama Canal, or battling some great storm on his way to Hawaii. When he left, I knew we wouldn’t see him again - the smoking and drinking had caught up with him, and he talked of “big plans.” Then, in my college years, I assumed he was finally sailing around the world.

The shopkeeper emerged from a doorway in the back brandishing an unlabeled tin. His face was lit up as he handed the stout man the tin, and realizing I was waiting there, he held up a finger, "I'll be with you in one moment." They talked quietly while the man emptied his bowl and packed it with this new leaf.

The summer after he left, my mom, my sister, and I drove to Florida where his house sat neglected. In the excruciating summer heat, we went through my grandfather’s things, settled his debts, and closed his accounts. There, I found some correspondence he had had with a colleague from Mumbai. The letters were vague, but they mentioned something about picking up the “product.”

A year later, a headline in the paper read _“Tons of Illegal Tobacco Smuggled from Mumbai; No Suspects Found.”_ My mother wanted badly to put it all behind her, and I couldn’t blame her. However, I couldn’t leave his story incomplete. When I graduated from college, I left to find my grandfather, the smuggler.

The shopkeeper approached me now, done with the stout man, and said, “what can I do for you today?”

I explained why I had come down to The Bahamas: my grandfather’s obsession with tobacco, his reckless mission to liberate it, and the leads I followed up and down the coast. I had to know what happened. Upon giving the shopkeeper his name, he grew quiet, and clearly uneasy.

“Your old man used to come around, bought cartons of tobacco for his trips. He had fine tastes, but... well, the stuff he bought got banned. I haven’t seen him since. Sorry kid.”

I asked the man if my grandfather ever talked about his “big plans,” or if he knew anything about the smuggling. He only shrugged, clearly wanting me to leave.

The stout man lit his bowl, and as I went for the door, the smell finally hit me. Memories rushed back like the ocean winds that swept him away. I turned to the shopkeeper and said, “I know that smell. That’s the stuff from India. It’s the only tobacco he smoked.”

The shop was silent. The stout man stopped his puffing in the back and turned to look at us. The shopkeeper’s eyes became slits and his brow furled, and I knew something wasn’t right. All the pieces lay in front of me, I just needed to put them together.

“You _do_ know where he is,” I said, at first excited, then desperate. “Tell me!” Here I was, at the end of the road, the last stop before the sea, and I’d be damned if I left before learning the truth.

The shopkeeper sighed and nodded to the stout man, who got up to leave. “Come with me.” The shopkeeper led me through the curtained doorway to a room in the back of the shop. There was a couch in the room, and he motioned for me to sit. “Your grandfather and I go back a long ways,” he started.

As the man talked, I noticed on the shelf in front of me was an ornate ivory urn sitting next to a black-and-white picture of the shopkeeper with my grandfather at the helm. They were both smiling and smoking their pipes, perhaps thirty years ago.

“Your old man said we could get more leaf than could be smoked in a lifetime. Since they banned the stuff, it’s been impossible to find. He said let’s go to India, we could bring it back and make millions, and he’d finally take his trip round the world.” The shopkeeper looked sad, forlorn.

I asked the shopkeeper when he had died. Did he walk off that boat, or did he come back as ashes?

The man sat in a chair across from me. “That’s the thing,” he said, reaching for his boot. “If he’d walked off that boat, we’d a had to split the profits three ways. Man was about to kick the bucket anyways, so…” he drew a slender knife from a holster on his leg. “We crewed together over thirty years, him and me. You kill your best friend for profit...” tears flowed from his eyes now, “you pay the ultimate price.”

I leapt off the couch a second too late. Blood sprayed onto my jeans and the man slumped forward, his neck oozing red. The knife gave a clank as it fell to the floor, and my grandfather’s tale had found its ending.
