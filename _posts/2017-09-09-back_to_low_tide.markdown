---
layout: post
title: Back to Low Tide
date: Sept 9th, 2017
categories:
  - writing
  - stories
description: |
  It is low tide in the bay. After a raging party on the water, a group of
  people sailing back to the city face unexpected consequences as they realize
  there is a thief among them.
introduction: |
  I'm fortunate enough to have an excellent brother who lives on a sailboat. He
  often invites me along on day sails, and the occasional overnight trip. This
  story was inspired by one such trip in which we accidentally strayed out of
  the channel on a windy day during the low tide and ended up aground for the
  rest of the afternoon. The water was shallow enough for our knees to stay dry
  as we explored a nearby island that had formed when the tide receeded.

  Some weeks later, I wrote down the beginning of this story: a scene in which
  adventurous ravers have a party on this ephemeral island. From there, I
  looped in several characters and a compelling conflict I had considered quite
  a bit earlier: what would happen if something valuable was stolen from
  somebody while in close confines of other people?

  To be honest, I don't like this story much. I don't like that the characters
  don't work together, and I don't like that they end up hating and distrusting
  one another even more than at the outset. I consider this great practice in
  writing dialog, people, situations, and action sequences, but if I had to do
  it over again, I would discard all but the beginning scene and change the
  genre to one of magical realism and mystery in a strange place on the water.
---

The generators were brought out first, lashed down to wooden shipping pallets and fixed with inner tubes for floatation. They were unloaded from dinghies as the first crown of mud peaked over the receding water, and the sun’s rays disappeared, barely ceasing their struggle over the distant horizon. Then out came the speakers, and through an amplifier, 20,000 watts of finely tuned beats pumped through the night, blasting in concert to a dance of LEDs. Kayaks and dinghies and canoes flocked to the island like moths drawn to a flame. A crowd formed on the small muddy enclave, growing with the ebb of the tide as the music played, cushioned by the remote stillness of the bay all around them.

Several times each year, just hours before midnight, the tide dropped low enough, and with the same absolute determination, they all come to greet the occasion. The bay, surrounded by oil refineries, was shallow enough to walk in except for a distant shipping lane leading to the delta. The wind blew cold and crisp, and as the sun hid behind the distant trees, you could see the running lights - red on port, green on starboard - approaching swiftly, hurriedly, for there wasn’t much time.

Lewis stood on the bow of Wanda’s boat. The waves chopped and tossed the bow around, and his bare feet felt slippery against the deck. They had furled in the sails and prepared to lay anchor while they still had six feet of depth. Soon the tide would leave the boat lying on its side, but the anchor was a simple precaution in case they couldn’t make it back fast enough. “Dropping anchor,” Lewis shouted. He felt a brief tug as Wanda reversed the engine, and the chain slipped out of his hands, plopping into the dark brackish water. When he looked up, the party shown brilliantly against a curtain of night, quickly filling out the newly created land.

He felt the 80 foot mark pass through his fingers, and Lewis tied off the anchor line. Wanda kept the boat in reverse until the hook caught, and then cut the engine. She took a double shot from her Sailor Jerry, and handed the bottle to Lewis.

“You think Ammy’s going to be there?”

Lewis shook his head. “Part of me hopes she will be.”

“What are you gonna say to her?”

He only shrugged and handed the bottle back to his friend. The alcohol burned away his anxiety, but left little in its place.

“Just so you know, I offered to bring Jack back to the city with us. You know they’re together now, right?”

His stomach sank like a rock, and his chest turned inside out. Ammy was a special person, he couldn’t deny her that. In a way, he knew he’d always love her despite her wild nature, but he knew what Jack was after and what he valued, and he hoped she’d learn quickly that it wasn’t good enough.

He just nodded. Said nothing.

Together they lifted the dinghy off the deck and into the water below. Already, Lewis started to feel the unnatural rigid rocking of a boat that had sunk its keel into the mud. They were beached, which meant the island had grown another couple hundred square feet.

***

They reached the island and dragged the dinghy onto the sand. It was soft and squishy between his toes, but the firmness of land made him dizzy. The two friends locked eyes for a moment, then hurried towards the pulsing masses gathered towards the center.

Mud flitted against his back as he ran towards them. Wanda handed him some glow sticks she had just acquired, and Lewis, intercepted by a goggled person wearing bleached pointy hair, accepted a glow-in-the-dark smiley-face painted onto his chest. As he stood there, barely ten feet from where they danced a radiant glow, a joint or something made its way into his hand, and he took a long smooth drag. A warmth filled his body and illuminated his senses, and he bolted in to join them.

There she was, Ammy, dancing not far from him. Her fluorescent yellow hair swayed back and forth, glimmering like diamonds in the night. Her eyes were closed, and somebody stood in front of her, knees bent, licking her exposed nipple. He felt the urge to tackle the bastard, but this wasn’t that kind of place. He resisted jealousy and kept telling himself he was over her. She caught his eye, waved at him with a quick smile and flick of her pinky, and disappeared into the sea of bodies. _Let her go_, he thought, closed his eyes, and let the beats overtake him.

***
Ammy was short, but agile, and her presence commanded people to let her through. She pushed through the tangle of bodies towards a long spit of raised land at the back end of the island, and there she found him. Jack stood in the dark, shaking the hand of a silhouette and stuffing something into the main pocket of his backpack. The moon glowed behind him and accented his slender waist and chiseled chest. When he looked at Ammy, the silhouette wandered off, back to the party. “What goods did you get this time?” she smiled and wrapped her arms around his neck. “Kiss me,” he commanded, and their lips touched, creating a seal as their mouths opened and their tongues embraced. She felt a slimy capsule make its way across the tongue-bridge and into her mouth. His heart beat straight into her. His arms kept her warm. “Tastes good, you _devil_,” she said as they detached, and her tongue started to tingle. “Never as good as you,” he replied.

They walked to the end of the spit where their feet disappeared under water. “I saw Lewis,” she said. “He’s here.”

“So what?” he replied, his eyes focused just below her face.

“Just thought you should know, you know…” She trailed off. “No surprises, right?”

“Thanks Babe, but I can handle him.”

“But can you handle _yourself?_” She slapped him playfully in the face.

He moved his fingers down her sides and worked them underneath her waistband. “Maybe I’ll handle _you_.” He said, the firmness of his voice echoed by his grip.

“Wait!” She whispered, and looked around. It was dark and silent where they were, and only the waves heard them. “Wait ‘till it kicks in!”

“We can do it again,” he said. “It’ll last for hours.” And he lowered her down and took her, there where only the waves could witness.

***

Some of them called it The Pit: the dense tangled mess of painted bodies, all moving as one synchronized beast. Wanda thought the name appropriate; once you joined, it was a struggle to leave. She came to watch them pulse and throb and meld into a single being, and she stood alone with her thoughts on the sidelines. She wandered over to a small crowd near the lights and the generators. Someone handed her a beer without asking, and she nodded in thanks. Without the blanket of people, the wind chilled her, tosser her hair around, and roared into her ear.

“Good night tonight, ain’t it?” remarked a dark-haired brown-eyed guy hooking up another LED strip. His hair was wavy over his forehead, and ballooned out over his ears in a Beatles sort of way. He was remarkably regular-looking for this sort of event.

“Sure is,” she replied. “It’s Willy, right?”

“That’s me,” he said. “You’re familiar, but I forgot your name.”

She told him her name, and they shook hands.

“Aren’t you the one with the boat?” He asked.

She nodded.

“I remember you from the raft-up last month. You brought that zipline, tied it to your mast.”

She recalled that day and cracked a smile. “I’ve got to say, you’ve got some balls under those shorts. That was highly inadvisable.”

He chuckled, and said, “you lied, didn’t you? You’d never done that before.”

She gave him a half-nod and raised her beer. “Someone’s gotta be first. They’ll call you a hero. Now we’ve got a way to get you back to shore if we’re too drunk to row.”

“The Hero With the Broken Neck,” he proclaimed.

They talked through another beer, and through the sound of the generators and the increasing volume of the music. Willy finished the last tie on the LED strip and the lights came on in a dancing colorful pattern that synchronized with the music. Wanda stared into the mesmerizing lights, and almost felt the urge to dance. She realized why she came to these parties. From outside The Pit, the view was better, the people more interesting, and the cutting wind more fresh.

“I programmed it myself,” he said, breaking her gaze. “They want to bring it up the delta after this. There’s another party getting started in a few days, a floating island festival.”

“Sounds fun,” she replied.

He shrugged. “Sure does. I’ve got to get back to the city though.”

“How are you getting there?”

“Dunno yet. I figure somebody around here is headed that way.”

She liked him well enough, and could use some company that wasn’t tangled up in romantic tension. “I’ve got to drop off a few friends at the city, you’re welcome to come along, but we’ll probably stay a night or two out here, take our time.”

His eyes opened wide in agreement. “If you’ve got room, that’s good with me,” he said.

“We’ll make it work,” she replied. “I’ll find you before we leave.”

***

Wanda wandered off along the shoreline. The wind created a chop in the water that slapped against her bare ankles, but they weren’t cold. She looked out along the water, to the refineries lighting up the distant landscapes, to the lights, slanting with the masts of beached sailboats. Somewhere out there the police were keeping eyes on them, but they wouldn’t do anything. No, they were here in case something bad happened. Nobody wants an idiot drowning out here.

Walking the perimeter of the island felt like touring her own kingdom. There was plenty of empty space, despite the hundreds of people enjoying the island’s temporary existence. Many of them, like her, peeled away from The Pit, creating pits of their own. She passed drum circles, bonfires, dancers spinning flaming poi. But mostly she enjoyed the quiet spaces along the edges where all she could hear was the water and the wind and her thoughts.

And slowly the water level rose, climbing up her ankles, and then her calves. There was plenty of time left, but people began to peel away and fetch their kayaks and dinghies. Each time, vessels were lost to the rising water, and she decided it was time to find her crew.

***

The dinghy took the five of them back in two trips. By the time Ammy, Jack, and Willy returned, Wanda’s boat was floating again, and Lewis stood at the bow preparing to winch the anchor back in. Wanda had the engine started and helped the three on to the boat, then she pulled Ammy and Jack aside.

“I don’t want any trouble here, ok? I want you to leave Lewis alone tonight.”

Ammy still felt the glow from her kiss with Jack earlier, and she spoke up first, “oh don’t worry, we have nothing but love for Lewis!”

“That’s what I’m worried about,” she replied.

“Your boat, your rules, Wanda. We’ll leave him alone,” Jack looked down on her, and his posture did nothing to reassure her. “I mean it, right Amanda?”

“Of course you do Sweetie!”

Wanda rolled her eyes and turned to Willy, who stood on the deck with an absent look about him.

“Go get comfortable and out of the way. Oh, bring me a beer, and grab one for yourself too.” Then she turned to the foredeck. “Lewis! We’re all set here, on my mark!”

With that, Lewis began cranking the handle, and the windlass tugged the boat slowly forward until the anchor chain was exposed, dropping muddy water all over the deck. Once he secured the anchor to the bow, he called out to Wanda, who shifted the engine into gear and moved them out to the channel.

The sails were up and the boat was under way. Lewis, Willy, and Wanda sat in the cockpit, drinking beers and trimming the sails, while Ammy and Jack stayed below.

“I told those two to leave you alone. Let me know if they start being idiots and I’ll throw them over or something.”

“Thanks,” said Lewis. “I’m fine, really.”

Her eyes said she didn’t believe him, but she let it rest. “Ok, there’s a marina up here. We’re going to tie up at the guest dock, but we’ll need to be out early tomorrow before the harbormaster’s shift starts. You guys are my crew,” her eyes passed back and forth to the two of them. “You cool with that?”

“Aye captain,” said Willy. “Don’t have much of a choice!”

Lewis simply nodded.

“Good. Once we tie up, let’s get some rest. I’d like to do some cruising around tomorrow since the wind will be good, and I know a spot we can go have some fun in the water.”

***

That night, they laid in the dark: Lewis in the quarter berth, Wanda in the full berth, Willy on the floor, and the couple on the pull-out bench. The waves pattered against the side of the hull, rocking the boat back and forth. Two inches from Lewis’s face, on the other side of the hull, the fenders squealed against the dock, buffers that saved the boat’s fading paint job. The unfamiliar sounds created a silence that seemed fake, but rhythmically natural. In a foggy dreaming awareness he heard shuffling, rummaging, digging, and as quickly as it had emerged, his awareness collapsed back into the darkness of sleep.

***

“There’s a coffee grinder hanging above the sink. You’ll find some beans in the cupboard,” she pointed towards Willy, who looked like he needed something to do. “You can boil some water on the stove while we get out of here.”

Lewis was on the dock, bowline in one hand and stern in the other, ready to cast them off. The water was calm, like it was resting after the wailing chop from the night before. A yellow sliver of sunlight erupted from behind the trees to the east, and the light blue skies of early morning invited the circling gulls to their breakfast. They circled overhead on a thin breeze that promised more excitement in the day to come.

Wanda had the engine running, and shouted at him to cast them off. She brought the boat forward and he fended it away from the dock with his hands, running alongside and hopping on before it drifted too far. They navigated around a series of turns that winded through rows and rows of docked boats, then passed through the narrow opening of the breakwater, and out of the marina.

“What do you do in the city?” Wanda asked, directed at Willy.

Willy shrugged. “A lot of stuff. I build light fixtures, sound systems. I DJ around town.”

She nodded at him. “Clubs, or bars, or…?”

“Both. I do a lot of nightclubs and warehouse parties, but sometimes bars or special events.”

“I guess you must be something then, huh?”

“Some people seem to think so.”

Lewis laughed sarcastically under his breath, and they both turned towards him but didn’t say anything. Awkwardness hung in the air as none of them knew where to take the conversation. Then Willy asked if Wanda lived in the city.

“Sometimes,” she replied. “I live on my boat.”

“That’s cool,” he said, and brushed his hair out of his eyes. “Which marina? Or do you just, like, sail around a lot?”

“I’m between marinas right now, actually. I’ve got a buddy who works at the boatyards though, he lets me tie up there most nights. Real salty crew that hangs out there though.”

He looked at her quizzically, not quite sure what that meant. Lewis offered no help, but sipped his coffee, amused by this strange newcomer.

“If you’ve got some time when we get back, we’ll get a drink at the bar and I’ll show you what I mean.”

Willy nodded in agreement. “Deal,” he said.

After they had put some distance between themselves and the marina, Wanda started, “let’s get those sails up. Lewis, can you show Willy what to do?”

Lewis rose, quietly observing Willy, who looked eager to help. Moving up towards the mast, he motioned for Willy to follow. He showed Willy how to wind the halyard over the winch and grind it in. “Three wraps, clockwise, and don’t let the wraps get crossed, or we’ll have to cut the line.” Lewis produced a knife with a worn-out serrated edge. “Keep the line tight and crank it fast,” he instructed.

Lewis stood on the other side of the mast where he could feed the bolt rope on the luff of the sail into a thin groove on the mast. When he called out, Willy began turning the winch handle and the sail rose quickly and smoothly, and it filled in nicely. They felt their speed increase as the sail added power.

Wanda cut the engine off, and they settled back into the cockpit. From there, Lewis unfurled the jib, and it took a nice shape.

They headed west, under a bridge which led into a narrow strait where the wind immediately picked up. The wind blew over the sails and the boat took on a steep heal, leaning hard to starboard. There was some shuffling from down below, a thud and an “Oh shit!”

“Alright down there?” asked Wanda.

“Yeah,” Ammy replied. “Just fell out of the bed.”

“Happens.”

Normally Lewis would have found this funny. Normally he would have rushed down to coddle her, offer her a mimosa to dull the pain that didn’t really exist. Or he would have remained in bed with her - a rare moment of privacy - while the rest of the crew was up above.

But not today. Not anymore.

Ammy’s head appeared. Her yellow hair hadn’t woken up, but still it blinded them like the sun that rose to their stern. “Morning!” She had a grin that would eat you up. Her teeth glistened and shined in the spotlight of her face. Without hesitation she hopped up on deck and held the shrouds to keep her balance.

“It’s so bright out here! There’s so much water! Careful of these cliffs Wanda!”

“Shut up girl, I know what I’m doing,” Wanda replied, feigning annoyance. “Come sit down, you make me nervous up there.”

Ammy was obviously still under the influence; the stuff Jack found was strong and long-lasting - Wanda knew that well. Ammy complied, and with exaggerated care made her way across the boat and sat next to Lewis. “I miss you Lewis.”

Suddenly a shout erupted from below deck: “What the fuck? My stuff is missing!”

The silence chilled her flash. The hair on Wanda’s arms stood up straight, but she could only guess what he was talking about.

Jack stood in the companionway. As if the silence were a question, he answered with punctuated staccato speech. “I had a plastic bag. In my backpack. It had some _things_ in it; they were worth a lot of money. Last night, after boarding, I checked and it was there. Now it’s gone.” His eyes accused each one of them, one by one.

“What kind of things?” asked Willy.

Jack scowled at him, and shoved his finger in Willy’s face. “Don’t press me, boy. Did you do it?”

Wanda’s heart rate quickened. She knew Jack had brought hundreds, maybe thousands of dollars worth of hard drugs aboard her boat - he did this every time. She knew how he afforded his expensive car and his condo in the middle of the city. She also knew what he was capable of when angry, and she needed to get ahead of his temper.

“We’ll sort it out” said Wanda. “But first we need to tack, so can somebody get on the port side winch?”

Dutifully, Lewis grabbed the winch handle and prepared to bring the jib around while Jack stood gaping at them all. “Really?” he exclaimed. “Seriously! Somebody stole my drugs and my money, and you all just sit there? It was one of you bastards, there’s no other way!”

“Ready Lewis?”

“Ready.”

“Coming about.” Wanda tacked the boat to port and the sails came around. Everything shifted as the boat healed the other way.

Jack emerged fully from the companionway and stood at the front of the cockpit. “Ok!” he demanded. “Now we talk about my missing stuff.”

“Are you sure you didn’t drop it?” Willy said.

Jack looked like he was about to smack him across the face, but Wanda stepped in first. “Jack. This sucks, I know. But right now, we need to get out of this strait. The wind is picking up and there are rocks to both sides of us. We’ll have to tack again in a couple minutes, and probably once more before the water way opens up. I need everybody to focus on sailing right now. I promise, we’ll investigate your missing... _things_... as soon as we’re in a safer place.”

The anger was evident on his face, but he said nothing. He stormed back below deck and out of sight.

Wanda breathed a sigh of relief. Ammy stared at the companionway with her neck extended forward and her eyes dilated wide, and Lewis continued his usual sulking, although he seemed to be perking up. Willy might finally be catching on to the undercurrents of the situation.

He looked at Lewis. “So uhh, do you think he suspects you dude? Given, you know…”

“I don’t care, man,” he said. “Jack is an asshole, and I didn’t do it.”

Ammy left and climbed back down into the cabin.

“Alright boys, we’ll figure this out. It’s a beautiful day, let’s try to do some sailing and get home safely.”

It wasn’t clear whether she still planned for a day cruise. Lewis just felt like getting home, but didn’t have the stamina or the guts to ask her. He sat silently on the bench, wrapping himself in his arms to protect his skin from the biting wind, and took in the day. All around them, seagulls flapped and dove into the sparkling water, and several brown hawks stalked from further above. By now, the sun was a third of the way up the sky, which was still free of clouds. They would roll in from the west with the quickening breeze, which was growing into a gusty force that pulled the boat forward in its unforgiving grasp.

They tacked three more times before spilling out of the strait and into a larger body of water. They had to cross another bay before they’d even see the city, but with these winds there may be some daylight to spare. Wanda called for them to reef the main sail, which would reduce its surface area to compensate the overpowering wind.

Wanda asked Lewis to fetch some life jackets. The waves were getting rough and choppy, and tossed the boat around, spraying water all over the slippery deck.

With a bit more light in his eyes, Lewis saluted her and made his way down below.

Jack was laying unconscious on the bench with his arms wrapped tight around his backpack and the bottle of Wanda's Sailor Jerry in his hand, now empty. The heal of the boat meant that gravity had him tucked against the back wall. His shoes were on and his face was red, but Lewis couldn't tell if he was really asleep. Ammy sat on the opposite bench, picking her fingernails. She yawned and looked up at him.

“Jack is really mad,” she whispered.

“I know,” said Lewis. “I probably would be too.”

“It wasn’t you, was it?” She gave him a look that betrayed her free-spirited naivety. It said _if it was, you’d better fess up_, because bad things are about to happen.

“Of course not,” he said. “What would I want _his_ drugs for?”

Lewis found three life jackets in the closet and brought them back up. He untied the reef lines, then instructed Willy back to the halyard. “Lower slowly,” he said. “Stop when I say so, then tie it off. Got it?”

“Yeah.”

As the sail came down, Lewis pulled on the first reef line until it was tight against the boom. “There!” he said, and tied it to its cleat.

Then he heard her shout, “Lewis!”

When he looked up, Jack was lunging at him with hands like claws and muscles like rocks. His eyes were made of rage, yet they seemed focused, like he had thought it all through. Lewis felt the claws - no, hands - close around his throat, and then he was pinned to the high side of the deck, grasping at the lifelines. Willy was struggling to tie off the halyard, and in that moment all Lewis wanted to do was yell at him to finish the knot before he tried anything.

Of course that’s not what he did.

Wanda watched it happen from behind the wheel. Lewis went down. He was hidden behind the cabin, but she saw him struggle under the weight of Jack’s lumberjack torso. She began to turn the boat into the wind, where it would level off and come to a stop, but Willy didn’t hesitate long enough. He turned away from the mast and began tugging at Jack, trying to pull him off the poor bastard on the deck. Meanwhile, the cleat hitch that Wanda was sure he didn’t know how to tie, slipped off, and the main sail dropped. She wasn’t prepared for the lee helm. As if right on cue, a gust blew and knocked the boat hard to port. Wanda had never broached her boat before, but she knew the feeling. The helpless inevitability, the moment that slowed time. The main sail was halfway down the mast and filled up like a pocket of wind. It pushed the top of the mast quickly through the air while the keel struggled to catch up in the sluggish water. The boat tipped onto its side, and Willy fell backwards. She held firmly to the wheel, and she saw Jack now grasping the grabrail, refusing the same fate as Willy.

As the wind spilled off the jib, the boat began to right itself. Depowered now, it continued to swing downwind. In his fall, Willy had grabbed a stanchion, but was halfway submerged in the water. Ammy offered him a hand back on to the boat. Lewis was back on his feet and had his knee planted firmly into the back of Jack’s neck, but Wanda yelled: “not now, Lewis! I need you.”

His eyes pleaded to her, but only for a moment. “Fuck this asshole! How can you let him on your boat?”

“What am I going to do, throw him overboard?”

He hesitated. Willy rolled back onto the bench, drenched and shivering, and Ammy looked Lewis in the eye. “I _told_ you something bad was going to happen! Now look!”

“Fuck you!” he said. “I didn’t steal your shit.” He stood up and kicked Jack in the side of the head. Then he kicked him again, and Wanda was shouting his name and Ammy was pulling him down off the deck. Lewis spat and left Jack facedown on the deck. “Fuckers,” he muttered and shoved Ammy away.

Wanda had the engine on and was struggling to furl the jib back in. “Could you?” She said. When he didn’t move, she added “please.”

Lewis snatched the winch handle and cranked it hard, slowly bringing the sail, which was heavy with wind, back into a tight curl at the front of the boat. Then he hopped up on deck, nearly tripping over Jack, and pulled the main sail the rest of the way down. It remained in a bunch, a tangle of dacron on the deck, folding over Jack who now began to stir. He turned his head and revealed a bloodied, possibly broken, nose.

The lee helm had pushed the boat around, and now they motored back towards the strait. Jack tried to get up, but Lewis had his foot planted firmly on his back and was fingering the knife in his pocket. “You stay down, you piece of shit,” he said.

Ammy, seemingly clear-headed, said “Lewis, get off him! Jack, you won’t hurt anybody, right?”

They passed under the bridge and the waterway grew narrow again. The cliff walls stood menacingly on each side, watching them, giving them only one way to go. _Back._

“Where are we going?” asked Willy, suddenly.

Wanda said nothing. She held her gaze with a commitment to focus. She didn’t reply when Jack demanded his stuff back. She didn’t budge when Ammy pleaded for a first-aid kit. She held fast to some destination back from where they came.

Under motor power, they didn’t have to tack. They traveled close to seven knots, much faster than their earlier upwind sailing, but the time seemed to pass much slower as the group sat in deep tense silence.

***

Jack and Ammy sat together at the bow of the boat. In a rare moment of vulnerability, he let her hold him in her lap. “You promised me you would be cool with him,” she said. “What were you even thinking?”

“I don’t know,” he said. “I thought I could get it out of him.”

“Get what? You don’t even know if he took your shit. I mean, do we even know Willy? What if it was him?”

Jack had nothing to say. His nose throbbed between his thumb and index finger, and he felt his face bruised and swollen. When he spoke, his voice came out clogged and muted, like he had the worst cold in his life. But he managed to ask, “where are we going, Ammy?”

“We could ask,” she said. “But I think you should apologize first.”

His eyes grew wide and he sat up, and the pounding in his face amplified. Wincing, he began to dispute her, but sensed he was headed for defeat.

“I love you babe, but you’re not getting out of this easily. Lewis is my friend. Wanda is my friend. You’re my friend, but I want you to man up and do this.”

She thought it would be a miracle if it worked. She really just wanted to lay down with some music and ride out her afterglow, but she knew when shit got real you have to deal with it.

So she managed to help him up, and together they stumbled back towards the cockpit, towards Wanda and Lewis, who was holding a life jacket.

Jack, holding his nose, started to speak, but Wanda cut him off.

“Look who it is, dripping blood all over my boat. You’re lucky you don’t have to wait any longer, we’re here.”

Ammy had already caught on. “Wanda…” she started cautiously.

Jack turned and looked up, following her eyes to a familiar place. Familiar, except the sun shone overhead and the birds were out, and this time, the island was empty. A full tide cycle had passed over night and during the day, and the mud lay exposed once again. Wanda pulled a knife from her waist pocket, it’s serrated edge good enough to cut rope, and skin. Lewis shoved the life jacket towards Jack. “You’ll want this,” he said.

Ammy protested. She pleaded, and lunged at Wanda with her fists out and her teeth bared. With one firm slap, Wanda sent her straight to the ground. She had her knife at the ready, and Lewis was prepared to back her up.

“You will get off my boat right now,” she said to Jack. She said it with full authority and conviction, like a police officer putting somebody on the ground. Veins popped out of her neck, but her eyes cut like blades and her hand held steady.

The time had passed for Jack to fight her off. He realized the only way to hold fast to his dignity would be to step off the boat, lest he walk in to her knife, and probably the one hidden in Lewis’s pocket. Without any motion towards Ammy, he snatched the life jacket from Lewis’s hand, put it on, and jumped off the boat.

Ammy shouted at him to stop. She punched Lewis in the chest and charged at Wanda, but she was stopped as easily as she was seduced.

“Let me go with him,” she said to Wanda at last, but it was too late. They motored back towards the city at full speed. Jack stood on the island and watched the distance grow between them. He held his hands high in the air, flashing obscene gestures for all to see, but Wanda and Lewis kept their focus forward, uncaring.

They crew were silent for a while, but eventually Ammy demanded, “what’s going to happen to Jack? You can’t just leave him there to freeze or drown.”

“There are plenty of boaters around there during the day. He’ll be picked up by someone.”

“How can you be so sure?”

Wanda’s scowl said _I know what I’m talking about, you little bitch_.

At some point Lewis realized Willy had been silently watching everything unfold. His face was hard to read, displaying a silly grin somewhere between a confused algebra student and a teenage boy who had just received his first kiss, and might get a little something more if he played his cards right. They had long exited the strait, crossed the bay, and the lights of the city were beginning to cut through the thick fog and illuminate the darkening sky. “What’s going on, Willy?”

He turned to Lewis and grinned. “Don’t you care about what happened to Jack’s stuff?”

It had become a sensitive matter that they hadn’t brought up. Wanda was upset that her boat broached, and Lewis was bruised with a pulled back. They had opened beers and tried to forget about it, honestly hoping that Jack had lost it overboard. But the taboo was back in the air, and Lewis could only shake his head and hope it went away again. “No, I don’t really care,” he said. “I’d be happy to never hear of this again.”

As if he had just won the golden ticket, Willy smiled with genuine delight. The whole trip had been worth it for him in the end, apparently. He stood up, patting a bulge in his pocket, and started towards the companionway. “Good,” he smiled once at Lewis, once at Wanda, and disappeared back into the cabin of the boat.

![sailboat](/assets/sailboat.jpg)
