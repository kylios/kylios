---
layout: post
title: Christmas Books, 2018
date: December, 2018
categories:
  - projects
  - bookbinding
description: Books I made for Christmas gifts in 2018
---

In 2018, I picked up bookbinding as a hobby. After taking a class and watching a ton of YouTube videos, I bound a few books as Christmas gifts for friends and family.

<blockquote class="imgur-embed-pub" lang="en" data-id="a/QCdUAPp"><a href="//imgur.com/a/QCdUAPp">Christmas 2018 books</a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>

I have to give a shout out to [Sea Lemon](https://www.youtube.com/user/SeaLemonDIY) on YouTube for being an excellent resource for this hobby.