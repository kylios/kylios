---
layout: post
title: Artemis, by Andy Weir
date: Oct 27, 2018
categories:
  - writing
  - book_reviews
description: |
  My book review of Andy Weir's novel: Artemis
introduction: |
  Updated from my [review on Goodreads](https://www.goodreads.com/book/show/34928122-artemis) (originally Jan 31, 2018).
---

I quite enjoyed _The Martian_, and many elements of that novel are present here, too. From the well-thought-out setting to witty and fun characters, Artemis seems like a great follow-up. Weir did his homework when it came to the science - no surprise there. The setting was also compelling, and well thought-out. Artemis, the city on the moon, is a relatively lawless and market-driven settlement. This works because... well, various reasons that seem plausible enough, and it provides a very nice set-up for the story: a high-stakes heist executed by Jazz, a long-time resident of the colony. While the story itself is enjoyable and enough to keep the pages turning, I had to suspend my disbelief at certain points when Jazz would do something that seemed to compromise the safety of everybody on board the station, breaking well-established protocols whose importance she had stressed earlier in the narrative.

It was nice to see Andy Weir's evolution as a writer. This book takes lots from _The Martian_, and tries to do quite a bit more. Jazz felt like a real person, with a real past and real problems. That said, the writing style was cringe-worthy at times, overly sarcastic and witty, which was difficult to ignore. Still, I think Andy Weir has a lot of potential for awesomeness, and has shown with _Artemis_ that he can execute a well-thought-out plot in a compelling situation with characters that feel almost real.
