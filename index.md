---
layout: post
permalink: /
comments: false
---

Hello,
=====

I'm Kyle Racette. I work as a software engineer, but I also enjoy writing, reading, cycling, bookbinding, and hiking, among other things. You might talk to me about science, nature, technology, books, history, music... anything really. So far I've enjoyed working as a software engineer in education technology, online video games, and aerospace.

I currently live in Oakland, California, but am originally from the East Coast.

Here you'll find some general information about me, as well as various things I've written and projects I've decided to share. Please see the [contact page](/contact) if you wish to get in touch, and thanks for visiting!
