---
layout: posts
permalink: /projects/
list_title: Projects
display_category: projects
menu_items:
  - bookbinding
  - programming
exclude_categories:
  - projects
---
