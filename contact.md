---
layout: default
permalink: /contact/
---

Contact
=====

You can find me on various social channels. If you'd like to contact me, I prefer that you use email, as I try to limit my use of social media.

> My email address is kracette (at) gmail (dot) com
>
> <a href="https://www.facebook.com/kracette" target="_blank">facebook</a>
>
> <a href="https://twitter.com/kyliocentricity" target="_blank">twitter</a>
>
> <a href="https://medium.com/@kylios" target="_blank">medium</a>
>
> <a href="https://gitlab.com/kylios" target="_blank">gitlab</a>
>
> <a href="https://github.com/kylios" target="_blank">github</a>
>
> <a href="https://goodreads.com/kylios" target="_blank">goodreads</a>